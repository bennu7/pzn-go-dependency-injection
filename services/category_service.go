package services

import (
	"context"
	"gitlab.com/bennu7/pzn-go-rest-api.git/model/web"
)

/*
 *terlebih dahulu buat kontrak interface service, seperti repository
 *jumlah catory service yang dibuat, biasanya mengikuti jumlah api nya
 */
type CategoryService interface {
	CreateCategory(ctx context.Context, request web.CategoryCreateRequest) web.CategoryResponse
	UpdateCategory(ctx context.Context, request web.CategoryUpdateRequest) web.CategoryResponse
	DeleteCategory(ctx context.Context, categoryId int)
	FindByIdCategory(ctx context.Context, categoryId int) web.CategoryResponse
	FindAllCategory(ctx context.Context) []web.CategoryResponse
}
