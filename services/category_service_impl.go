package services

import (
	"context"
	"database/sql"
	"github.com/go-playground/validator/v10"
	"gitlab.com/bennu7/pzn-go-rest-api.git/exception"
	"gitlab.com/bennu7/pzn-go-rest-api.git/helper"
	"gitlab.com/bennu7/pzn-go-rest-api.git/model/domain"
	"gitlab.com/bennu7/pzn-go-rest-api.git/model/web"
	"gitlab.com/bennu7/pzn-go-rest-api.git/repository"
)

type CategoryServiceImpl struct {
	//	*butuh repository, karena manipulasi data akan dilakukan di repository
	CategoryRepository repository.CategoryRepository
	DB                 *sql.DB
	Validate           *validator.Validate
}

// *buat constructor setelah semua interface dan method jadi
func NewCategoryService(categoryRepository repository.CategoryRepository, DB *sql.DB, validate *validator.Validate) *CategoryServiceImpl {
	return &CategoryServiceImpl{
		CategoryRepository: categoryRepository,
		DB:                 DB,
		Validate:           validate,
	}
}

func (service *CategoryServiceImpl) CreateCategory(ctx context.Context, request web.CategoryCreateRequest) web.CategoryResponse {
	// *sebelum masuk query transaksional, letakkan validator
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	// *menggunakan transakional db untuk memastikan data yang di insert ke db, benar benar berhasil
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)

	//defer func() {
	// *jadikan wrap helper function, karena ini akan sering digunakan di semua method dalam file ini
	//	if err := recover(); err != nil {
	//		tx.Rollback()
	//		panic(err)
	//	} else {
	//		tx.Commit()
	//	}
	//}()
	defer helper.CommitOrRollback(tx)

	category := domain.Category{
		Name: request.Name,
	}

	// *sebelum return berakhir, kelola data pada service yang telah di buat
	category = service.CategoryRepository.Save(ctx, tx, category)

	return helper.ToCategoryResponse(category)
}

func (service *CategoryServiceImpl) UpdateCategory(ctx context.Context, request web.CategoryUpdateRequest) web.CategoryResponse {
	// *sebelum masuk query transaksional, letakkan validator
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	tx, err := service.DB.Begin()
	helper.PanicIfError(err)

	defer helper.CommitOrRollback(tx)

	// *terlebih dahulu cek data sebelumnya ada data apa tidak berdasarkan id tersebut
	category, err := service.CategoryRepository.FindById(ctx, tx, request.Id)
	//helper.PanicIfError(err)
	// *digantikan error handler dengan response status
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	// *setelah itu set data berdasarkan request
	category.Id = request.Id
	category.Name = request.Name

	category = service.CategoryRepository.Update(ctx, tx, category)

	return helper.ToCategoryResponse(category)
}

func (service *CategoryServiceImpl) DeleteCategory(ctx context.Context, categoryId int) {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	category, err := service.CategoryRepository.FindById(ctx, tx, categoryId)
	//helper.PanicIfError(err)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	category.Id = categoryId

	service.CategoryRepository.Delete(ctx, tx, category)

	helper.ToCategoryResponse(category)
}

func (service *CategoryServiceImpl) FindByIdCategory(ctx context.Context, categoryId int) web.CategoryResponse {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	category, err := service.CategoryRepository.FindById(ctx, tx, categoryId)
	//helper.PanicIfError(err)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	return helper.ToCategoryResponse(category)

}

func (service *CategoryServiceImpl) FindAllCategory(ctx context.Context) []web.CategoryResponse {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	categories := service.CategoryRepository.FindAll(ctx, tx)

	// *dibuat helper model agar lebih mudah implementasinya
	//var categoryResponses []web.CategoryResponse
	//for _, category := range categories {
	//	categoryResponses = append(categoryResponses, helper.ToCategoryResponse(category))
	//}
	//
	//return categoryResponses

	return helper.ToCategoryResponses(categories)
}
