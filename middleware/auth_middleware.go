package middleware

import (
	"gitlab.com/bennu7/pzn-go-rest-api.git/helper"
	"gitlab.com/bennu7/pzn-go-rest-api.git/model/web"
	"net/http"
)

type AuthMiddleware struct {
	// *karena Handler AuthMiddleware ini akan ditempatkan paling atas, dan otomatis ia meneruskan handler ke request berikutnya maka perlu ada Handler lagi
	Handler http.Handler
}

func NewAuthMiddleware(handler http.Handler) *AuthMiddleware {
	return &AuthMiddleware{Handler: handler}
}

func (middleware *AuthMiddleware) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if "wkwkLandAPIKEY" == request.Header.Get("X-API-Key") {
		// *ok, jika tidak ada masalah maka diperbolehkan lanjut
		middleware.Handler.ServeHTTP(writer, request)
	} else {
		//	error
		writer.Header().Set("Content-Type", "application/json")
		writer.WriteHeader(http.StatusUnauthorized)

		webResponse := web.WebResponse{
			Code:   http.StatusUnauthorized,
			Status: "Unauthorized",
		}

		helper.WriteToResponseOkBody(writer, webResponse)
	}
}
