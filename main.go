package main

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/bennu7/pzn-go-rest-api.git/middleware"
	"net/http"
)

func NewServer(authMiddleware *middleware.AuthMiddleware) *http.Server {
	return &http.Server{
		Addr:    "localhost:8080",
		Handler: authMiddleware,
	}
}

func main() {

	//authMiddleware := InitializedServer()
	//
	//router.PanicHandler = exception.ErrorHandler
	//
	//server := NewServer(authMiddleware)
	//
	//err := server.ListenAndServe()
	//helper.PanicIfError(err)
}
