package helper

import (
	"encoding/json"
	"net/http"
)

func ReadFromRequestBody(request *http.Request, result any) {
	decoder := json.NewDecoder(request.Body)

	//categoryCreateRequest := web.CategoryCreateRequest{}
	//err := decoder.Decode(&categoryCreateRequest)

	err := decoder.Decode(&result)
	PanicIfError(err)
}

func WriteToResponseOkBody(writer http.ResponseWriter, response any) {
	writer.WriteHeader(http.StatusOK)
	writer.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(writer)
	err := encoder.Encode(&response)
	PanicIfError(err)
}

func WriteToResponseCreatedBody(writer http.ResponseWriter, response any) {
	writer.WriteHeader(http.StatusCreated)
	writer.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(writer)
	err := encoder.Encode(&response)
	PanicIfError(err)
}

func WriteToResponseBody(writer http.ResponseWriter, response any) {
	writer.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(writer)
	err := encoder.Encode(&response)
	PanicIfError(err)
}
