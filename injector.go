//go:build wireinject
// +build wireinject

package main

import (
	"github.com/go-playground/validator/v10"
	"github.com/google/wire"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/bennu7/pzn-go-dependency-injection.git/controller"
	"gitlab.com/bennu7/pzn-go-dependency-injection.git/services"
	"gitlab.com/bennu7/pzn-go-rest-api.git/app"
	"gitlab.com/bennu7/pzn-go-rest-api.git/middleware"
	"gitlab.com/bennu7/pzn-go-rest-api.git/repository"
	"net/http"
)

/*
* implementasikan semua constructor/provider dengan wire
* semua data main akan dipindahkan selain ServerHTTP
* implementasikan db, validator,
* untuk Binding, update tiap construtor dengan kembalian sebagai constructor bukan sebagai interface => sebagai bahan pembelajaran
* tambahankan wire Build berupa db, validator, kumpulan function category mencakup service, repository serta controller
* dan tambahkan NewRouter, authMiddleware dan NewServer
* setelah itu generate wire
 */

var categorySet = wire.NewSet(
	repository.NewCategoryRepository,
	wire.Bind(new(repository.CategoryRepository), new(*repository.CategoryRepositoryImpl)),
	services.NewCategoryService,
	wire.Bind(new(services.CategoryService), new(*services.CategoryServiceImpl)),
	controller.NewCategoryController,
	wire.Bind(new(controller.CategoryController), new(*controller.CategoryControllerImpl)),
)

func InitializedServer() *http.Server {
	wire.Build(
		app.NewDB,
		validator.New,
		categorySet,
		app.NewRouter,
		wire.Bind(new(http.Handler), new(*httprouter.Router)),
		middleware.NewAuthMiddleware,
		NewServer,
	)
	return nil
}
