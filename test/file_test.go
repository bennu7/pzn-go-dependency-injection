package test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/bennu7/pzn-go-dependency-injection.git/simple"
	"testing"
)

func TestConnection(t *testing.T) {
	connection, cleanup := simple.InitializedConnection("database")

	assert.NotNil(t, connection)
	cleanup()
}
