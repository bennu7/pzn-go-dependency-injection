package test

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/go-playground/assert/v2"
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/bennu7/pzn-go-rest-api.git/app"
	"gitlab.com/bennu7/pzn-go-rest-api.git/controller"
	"gitlab.com/bennu7/pzn-go-rest-api.git/helper"
	"gitlab.com/bennu7/pzn-go-rest-api.git/middleware"
	"gitlab.com/bennu7/pzn-go-rest-api.git/model/domain"
	"gitlab.com/bennu7/pzn-go-rest-api.git/repository"
	"gitlab.com/bennu7/pzn-go-rest-api.git/services"
)

func setUpTestDB() *sql.DB {
	db, err := sql.Open("mysql", "root:Flypower167/\\@tcp(localhost:3306)/pzn_go_rest_api_test")
	helper.PanicIfError(err)

	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(20)
	db.SetConnMaxLifetime(60 * time.Minute)
	db.SetConnMaxIdleTime(10)

	return db
}

func setupRouter(db *sql.DB) http.Handler {
	validate := validator.New()

	categoryRepository := repository.NewCategoryRepository()
	categoryService := services.NewCategoryService(categoryRepository, db, validate)
	categoryController := controller.NewCategoryController(categoryService)
	router := app.NewRouter(categoryController)

	return middleware.NewAuthMiddleware(router)
}

func truncateCategory(db *sql.DB) {
	_, err := db.Exec("TRUNCATE TABLE category")
	helper.PanicIfError(err)
}

func TestCreateCategorySuccess(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	requestBody := strings.NewReader(`{"name" : "Fly"}`)
	request := httptest.NewRequest(http.MethodPost, "http://localhost:8080/api/categories", requestBody)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	//assert.Equal(t, 201, int(responseBody["code"].(float64)))
	assert.Equal(t, http.StatusCreated, response.StatusCode)
	assert.Equal(t, "Fly", responseBody["data"].(map[string]any)["name"])
	assert.Equal(t, "OK", responseBody["status"])
}

// !Failed
func TestCreateCategoryFailed(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	requestBody := strings.NewReader(`{"name" : ""}`)
	request := httptest.NewRequest(http.MethodPost, "http://localhost:8080/api/categories", requestBody)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.Equal(t, "Bad Request", responseBody["status"])
}

func TestUpdateCategorySuccess(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	// *terlebih dahulu buat data sementara, manfaatkan repository dengan transaksional
	tx, err := db.Begin()
	helper.PanicIfError(err)
	categoryRepository := repository.NewCategoryRepository()
	category := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Laptop",
	})
	tx.Commit()

	requestBody := strings.NewReader(`{"name" : "Macbook"}`)
	request := httptest.NewRequest(http.MethodPut, "http://localhost:8080/api/categories/"+strconv.Itoa(category.Id), requestBody)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	assert.Equal(t, http.StatusOK, response.StatusCode)
	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])
	assert.Equal(t, category.Id, int(responseBody["data"].(map[string]any)["id"].(float64)))
}

// !Failed
func TestUpdateCategoryFailed(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	// *terlebih dahulu buat data sementara, manfaatkan repository dengan transaksional
	tx, err := db.Begin()
	helper.PanicIfError(err)
	categoryRepository := repository.NewCategoryRepository()
	category := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Laptop",
	})
	tx.Commit()

	requestBody := strings.NewReader(`{"name" : ""}`)
	request := httptest.NewRequest(http.MethodPut, "http://localhost:8080/api/categories/"+strconv.Itoa(category.Id), requestBody)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.Equal(t, "Bad Request", responseBody["status"])
}

func TestGetCategorySuccess(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	// *terlebih dahulu buat data sementara, manfaatkan repository dengan transaksional
	tx, err := db.Begin()
	helper.PanicIfError(err)
	categoryRepository := repository.NewCategoryRepository()
	category := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Laptop",
	})
	tx.Commit()

	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/api/categories/"+strconv.Itoa(category.Id), nil)
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	assert.Equal(t, http.StatusOK, response.StatusCode)
	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])
	assert.Equal(t, category.Id, int(responseBody["data"].(map[string]any)["id"].(float64)))
	assert.Equal(t, category.Name, responseBody["data"].(map[string]any)["name"].(string))

}

// !Failed
func TestGetCategoryFailed(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	tx, err := db.Begin()
	helper.PanicIfError(err)
	categoryRepository := repository.NewCategoryRepository()
	category := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Laptop",
	})
	tx.Commit()
	category.Id += 1
	fmt.Println(category.Id)

	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/api/categories/"+strconv.Itoa(category.Id), nil)
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	assert.Equal(t, http.StatusNotFound, response.StatusCode)
	assert.Equal(t, 404, int(responseBody["code"].(float64)))
	assert.Equal(t, "Not Found", responseBody["status"])
}

func TestListGetCategoriesSuccess(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	// *terlebih dahulu buat data sementara, manfaatkan repository dengan transaksional
	tx, err := db.Begin()
	helper.PanicIfError(err)
	categoryRepository := repository.NewCategoryRepository()

	category := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Laptop",
	})

	category2 := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Smartphone",
	})
	tx.Commit()

	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/api/categories", nil)
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)
	// *tidak bisa langsung digunakan []map[string]any, gunakan []interface{}
	var categories = responseBody["data"].([]interface{})
	categoryResponse := categories[0].(map[string]any)
	categoryResponse2 := categories[1].(map[string]any)

	assert.Equal(t, http.StatusOK, response.StatusCode)
	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])
	assert.Equal(t, category.Id, int(categoryResponse["id"].(float64)))
	assert.Equal(t, category.Name, categoryResponse["name"].(string))
	assert.Equal(t, category2.Id, int(categoryResponse2["id"].(float64)))
	assert.Equal(t, category2.Name, categoryResponse2["name"].(string))
}

func TestListGetCategoriesFailed(t *testing.T) {

}

func TestDeleteCategorySuccess(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	// *terlebih dahulu buat data sementara, manfaatkan repository dengan transaksional
	tx, err := db.Begin()
	helper.PanicIfError(err)
	categoryRepository := repository.NewCategoryRepository()
	category := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Laptop",
	})
	tx.Commit()

	request := httptest.NewRequest(http.MethodDelete, "http://localhost:8080/api/categories/"+strconv.Itoa(category.Id), nil)
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	assert.Equal(t, http.StatusOK, response.StatusCode)
	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])
}

// !Failed
func TestDeleteCategoryFailed(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	// *terlebih dahulu buat data sementara, manfaatkan repository dengan transaksional
	tx, err := db.Begin()
	helper.PanicIfError(err)
	categoryRepository := repository.NewCategoryRepository()
	category := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Laptop",
	})
	tx.Commit()
	category.Id += 1

	request := httptest.NewRequest(http.MethodDelete, "http://localhost:8080/api/categories/"+strconv.Itoa(category.Id), nil)
	request.Header.Add("X-API-Key", "wkwkLandAPIKEY")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	assert.Equal(t, http.StatusNotFound, response.StatusCode)
	assert.Equal(t, 404, int(responseBody["code"].(float64)))
	assert.Equal(t, "Not Found", responseBody["status"])

}

func TestUnauthorized(t *testing.T) {
	db := setUpTestDB()
	router := setupRouter(db)
	truncateCategory(db)

	tx, err := db.Begin()
	helper.PanicIfError(err)
	categoryRepository := repository.NewCategoryRepository()
	category := categoryRepository.Save(context.Background(), tx, domain.Category{
		Name: "Laptop",
	})
	tx.Commit()

	request := httptest.NewRequest(http.MethodDelete, "http://localhost:8080/api/categories/"+strconv.Itoa(category.Id), nil)
	// request.Header.Add("X-API-Key", "wkwkLandAPIKEY")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]any

	json.Unmarshal(body, &responseBody)

	assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	assert.Equal(t, 401, int(responseBody["code"].(float64)))
	assert.Equal(t, "Unauthorized", responseBody["status"])
}
