package test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/bennu7/pzn-go-dependency-injection.git/simple"
	"testing"
)

func TestSimpleService(t *testing.T) {
	simpleService, err := simple.InitializedService(true)
	fmt.Println(err)
	fmt.Println(simpleService)
	//fmt.Println(simpleService.SimpleRepository) // !errornya failed invalid memory address or nil pointer dereference [recovered]
}

func TestSimpleServiceError(t *testing.T) {
	simpleService, err := simple.InitializedService(true)
	assert.Nil(t, simpleService)
	assert.NotNil(t, err)
}

func TestSimpleServiceSuccess(t *testing.T) {
	simpleService, err := simple.InitializedService(false)
	assert.Nil(t, err)
	assert.NotNil(t, simpleService)
}
