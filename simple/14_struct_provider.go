package simple

type Foo struct {
}

// *function provider
func NewFoo() *Foo {
	return &Foo{}
}

type Bar struct {
}

// *function provider
func NewBar() *Bar {
	return &Bar{}
}

// *di case ini tanpa menggunakan provider
type FooBar struct {
	*Foo
	*Bar
}
