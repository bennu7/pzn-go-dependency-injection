package simple

type SayHello interface {
	Hello(name string) string
}

type HelloService struct {
	SayHello
}

type SayHelloImpl struct {
}

func (s *SayHelloImpl) Hello(name string) string {
	return "Hello " + name
}

// *implementasi provider dengan interface wire
func NewSayHelloImpl() *SayHelloImpl {
	return &SayHelloImpl{}
}

// *implementasi provider dengan interface wire
func NewHelloService(sayHello SayHello) *HelloService {
	return &HelloService{SayHello: sayHello}
}
