package simple

import "fmt"

type Connection struct {
	File *File
}

// *cleanup function
func (conn *Connection) Close() {
	fmt.Println("Close connection ", conn.File)
}
func NewConnection(file *File) (*Connection, func()) {
	conn := &Connection{File: file}
	return conn, func() {
		conn.Close()
	}
}
