package simple

/*
* 11 Multiple Binding
* saat mendeklarasikan provider lebih dari satu dengan tipe yang sama, maka kita bisa menggunakan multiple binding
! tetapi masalahkan adalah kita tidak bisa menggunakan multiple binding untuk tipe yang sama dengan provider yang berbeda
? untuk mengatasinya gunakan alias pada struct yang akan di binding pada provider masing masing
*/

type Database struct {
	Name string
}

// *example : dengan alias, tidak akan error
type DatabasePostgreSQL Database
type DatabaseMongoDB Database

func NewDatabasePostgreSQL() *DatabasePostgreSQL {
	// *harus deklarasikan manual lagi dan return pointer dari alias dan database
	database := &Database{
		Name: "PostgreSQL",
	}

	return (*DatabasePostgreSQL)(database)
}

func NewDatabaseMongoDB() *DatabaseMongoDB {
	database := &Database{
		Name: "MongoDB",
	}

	return (*DatabaseMongoDB)(database)
}

type DatabaseRepository struct {
	DatabasePostgreSQL *DatabasePostgreSQL
	DatabaseMongoDB    *DatabaseMongoDB
}

func NewDatabaseRepository(postgreSQL *DatabasePostgreSQL, mongoDB *DatabaseMongoDB) *DatabaseRepository {
	return &DatabaseRepository{DatabasePostgreSQL: postgreSQL, DatabaseMongoDB: mongoDB}
}

//// *example : tanpa alias, akan error
//// !message error cli after run go generate : provider has multiple parameters of type *gitlab.com/bennu7/pzn-go-dependency-injection.git/simple.Database
//func NewDatabasePostgreSQL() *Database {
//	return &Database{Name: "PostgreSQL"}
//}
//
//func NewDatabaseMongoDB() *Database {
//	return &Database{Name: "MongoDB"}
//}
//
//type DatabaseRepository struct {
//	DatabasePostgreSQL *Database
//	DatabaseMongoDB    *Database
//}
//
//func NewDatabaseRepository(postgreSQL *Database, mongoDB *Database) *DatabaseRepository {
//	return &DatabaseRepository{DatabasePostgreSQL: postgreSQL, DatabaseMongoDB: mongoDB}
//}
