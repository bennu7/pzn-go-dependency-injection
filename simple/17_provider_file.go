package simple

import "fmt"

/*
* Provider file
 */

type File struct {
	Name string
}

func NewFile(name string) (*File, func()) {
	file := &File{Name: name}
	return file, func() {
		// *cleanup function di dalam provider
		file.Close()
	}
}

// *cleanup function
func (f *File) Close() {
	fmt.Println("Close file ", f.Name)
}
