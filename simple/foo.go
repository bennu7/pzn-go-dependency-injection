package simple

/*
* 12 Provider Set => Grouping Provider, sangat berguna jika kita memiliki banyak provider / constructor
* dan menggabungkan semua provider" yang telah dibuat sebelumnya
 */

type FooRepository struct {
}

func NewFooRepository() *FooRepository {
	return &FooRepository{}
}

type FooService struct {
	FooRepository *FooRepository
}

func NewFooService(repository *FooRepository) *FooService {
	return &FooService{FooRepository: repository}
}
