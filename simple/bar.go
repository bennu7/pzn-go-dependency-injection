package simple

/*
* 12 Provider Set => Grouping Provider, sangat berguna jika kita memiliki banyak provider / constructor
* dan menggabungkan semua provider" yang telah dibuat sebelumnya
 */

type BarRepository struct {
}

func NewBarRepository() *BarRepository {
	return &BarRepository{}
}

type BarService struct {
	BarRepository *BarRepository
}

func NewBarService(repository *BarRepository) *BarService {
	return &BarService{BarRepository: repository}
}
