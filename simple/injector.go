//go:build wireinject
// +build wireinject

package simple

import (
	"github.com/google/wire"
	"io"
	"os"
)

/*
* pada file ini tidak akan di eksekusi oleh compiler go, karena ini hanya untuk code generator google wire (Dependency Injection)
* untuk generate gunakan perintah go gen namaModule/namaPackage
* atau lebih gampang nya masuk ke folder dimana Provider dan Injector berada, dalam hal ini berada pada folder simple
* lalu jalankan perintah wire pada cli
* handle error, manfaatkan return kedua berupa error
 */

func InitializedService(isError bool) (*SimpleService, error) {
	wire.Build(
		NewSimpleRepository, NewSimpleService,
	)
	return nil, nil
}

func InitializedDatabaseRepository() *DatabaseRepository {
	wire.Build(
		NewDatabaseMongoDB,
		NewDatabasePostgreSQL,
		NewDatabaseRepository,
	)

	return nil
}

/*
* 12 lanjutkan provider set setelah membuat provider" pada foo, bar, dan fooBar dengan membuat inisialisasi service untuk menampung semuanya
* simpan provider"nya dengan menggunakan Provider Set dengan perintah wire.NewSet(newRepository , newService), isi parameter nya adalah provider" yang akan di gabungkan
 */

var fooSet = wire.NewSet(NewFooRepository, NewFooService)
var barSet = wire.NewSet(NewBarRepository, NewBarService)

func InitializedFooBarService() *FooBarService {
	wire.Build(fooSet, barSet, NewFooBarService)
	return nil
}

/*
* 13 Binding Interface, memanfaatkan interface ketika mendeklarasikan method dengan wire
* harus memanfaatkan wire.NewSet() dengan binding milik wire yaitu wire.Bind()
 */
// ?contoh penggunaan Binding Interface yang benar,
// ?manfaatkan pembuatan variabel provider set, wire.NewSet() namun harus di binding pada interface nya dan struct
var helloSet = wire.NewSet(NewSayHelloImpl, wire.Bind(new(SayHello), new(*SayHelloImpl)))

func InitializedSayHelloService() *HelloService {
	wire.Build(helloSet, NewHelloService)
	return nil
}

//// !contoh penggunaan Binding Interface yang salah
//func InitializedHelloService() *HelloService {
//	wire.Build(NewHelloService, NewSayHelloImpl)
//	return nil
//}

/*
* 14 Struct Provider
* tanpa menginisialisasi akhir provider function pada struct FooBar di file 14_struct_provider.go
* manfaatkan NewSet berisi kumpulan provider yang akan di gabungkan
* kemudiann Build dengan wire.Struct(new(FooBar), "Foo", "Bar") dengan kembalian pointer dari struct FooBar
 */
var FooBarSet = wire.NewSet(
	NewFoo,
	NewBar,
)

func InitializedFooBar() *FooBar {
	wire.Build(
		FooBarSet,
		wire.Struct(new(FooBar), "Foo", "Bar"),
	)
	return nil
}

/*
* 15 Binding Values
 */
var fooValue = &Foo{}
var barValue = &Bar{}

func initializedFooBarUsingValue() *FooBar {
	wire.Build(wire.Value(fooValue), wire.Value(barValue), wire.Struct(new(FooBar), "*"))
	return nil
}

/*
* 15 Binding Values, dengan interface value. io.Reader adalah interface
 */

func InitializeReader() io.Reader {
	wire.Build(wire.InterfaceValue(new(io.Reader), os.Stdin))
	return nil
}

// *16 struct field configuration, memanfaatkan struct Configuration sebagai provider bukan dari Application
func InitializedConfiguration() *Configuration {
	wire.Build(
		NewApplication,
		wire.FieldsOf(new(*Application), "Configuration"),
	)

	return nil
}

// * 17 provider connection and file
func InitializedConnection(name string) (*Connection, func()) {
	wire.Build(NewConnection, NewFile)
	return nil, nil
}
