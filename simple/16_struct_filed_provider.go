package simple

/*
* 16 struct field provider, memanfaatkan struct Configuration sebagai provider pada provider NewApplication()
 */

type Configuration struct {
	Name string
}
type Application struct {
	*Configuration
}

func NewApplication() *Application {
	return &Application{
		Configuration: &Configuration{
			Name: "Lalu Ibnu",
		},
	}
}
