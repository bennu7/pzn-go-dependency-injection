package simple

import "errors"

// *contoh menambahkan Error untuk bisa di handle dengan google wire
type SimpleRepository struct {
	Error bool
}

// *constructor dari google wire, => Provider
// *tambahkan parameter isError untuk bisa di handle dengan google wire
func NewSimpleRepository(isError bool) *SimpleRepository {
	return &SimpleRepository{Error: isError}
}

type SimpleService struct {
	*SimpleRepository
}

// *constructor dari google wire, => Provider
func NewSimpleService(repository *SimpleRepository) (*SimpleService, error) {
	if repository.Error {
		return nil, errors.New("failed create service")
	} else {
		return &SimpleService{SimpleRepository: repository}, nil
	}
}
