package simple

/*
* gabungkan service" yang telah dibuat sebelumnya dengan materi Provider Set, foo dan bar
* dalam hal ini maka keseluruhan jumlah provider/constructor adalah 5
 */

type FooBarService struct {
	*BarService
	*FooService
}

func NewFooBarService(barService *BarService, fooService *FooService) *FooBarService {
	return &FooBarService{BarService: barService, FooService: fooService}
}
